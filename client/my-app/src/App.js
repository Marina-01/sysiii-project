import React, { useState, useEffect } from 'react';
import axios from 'axios';

import './App.css';
import HomeView from './Views/HomeView.js'
import LoginView from './Views/LoginView.js'
import RegisterView from './Views/RegisterView.js'
import MyProfileView from './Views/MyProfileView.js'
import SearchPage from './Views/Search.js'
import Dashboard from './Views/Dashboard.js'
import NotificationPage from './Views/NotificationPage.js'
import {BrowserRouter, Route, Routes, Navigate} from 'react-router-dom'

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const token = localStorage.getItem('token');
    console.log('Token in localStorage:', token); // Debugging line

    if (token) {
      setIsLoggedIn(true);
      console.log('User is logged in'); // Debugging line
    } else {
      console.log('No token found, user is not logged in'); // Debugging line
    }
  }, []);
  return (
    <div className="App">
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<HomeView />} />
          <Route path='/sign-in' element={<LoginView />} />
          <Route path='/sign-up' element={<RegisterView />} />
          <Route path='/myprofile' element={<MyProfileView />} />
           <Route path='/dashboard' element={<Dashboard />} />
          <Route path='/notifications' element={<NotificationPage />} />
          <Route path="/search" element={<SearchPage /> } />
        </Routes>
      </BrowserRouter>
    </div>
  );
}

export default App;