import React, { useState, useEffect } from 'react';
import axios from 'axios';
import NavBar from './NavBar';
import './Dashboard.css'; // Your CSS file for styling

const Dashboard = () => {
    const [bookings, setBookings] = useState([]);
    const [error, setError] = useState(null);

useEffect(() => {
        const fetchBookings = async () => {
            try {
                const token = localStorage.getItem('token');
                if (!token) {
                    throw new Error('No token found');
                }
                
                const response = await axios.get('http://localhost:5000/notifications', {
                    headers: { Authorization: `Bearer ${token}` }
                });
                console.log('Response Data:', response.data);
                setBookings(response.data);
            } catch (err) {
                console.error('Error fetching bookings:', err.response?.data || err.message);
                setError('Error fetching bookings');
            }
        };

        fetchBookings();
    }, []);

    if (error) {
        return <div>{error}</div>;
    }

    return (
        <div>
            <NavBar />
            <h1>My Bookings</h1>
            <ul>
                {bookings.map((booking) => (
                    <li key={booking.id}>
                        <p><strong>Location:</strong> {booking.location}</p>
                        <p><strong>Number of Children:</strong> {booking.children.length}</p>
                        <p><strong>Status:</strong> {booking.status.charAt(0).toUpperCase() + booking.status.slice(1)}</p>
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default Dashboard;
