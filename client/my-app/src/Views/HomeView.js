import React from 'react';
import NavBar from './NavBar';
import './HomePage.css'; // Assuming you have some styles for the homepage

const HomePage = () => {
    return (
        <div>
            <NavBar />
            <div className="home-container">
                <section className="intro">
                    <h1>Welcome to TinyCare!</h1>
                    <p>
                        At TinyCare, we understand that finding the perfect babysitter is crucial for your family's peace of mind. 
                        Our platform connects you with highly qualified and trusted babysitters in your area. Whether you need occasional help 
                        or a regular caregiver, TinyCare makes it easy to find the right match for your child's needs. With our user-friendly 
                        interface and comprehensive profiles, you can make informed decisions and ensure your little ones are in good hands.
                    </p>
                </section>

                <section className="testimonials">
                    <h2>What Our Users Say</h2>
                    <div className="testimonial">
                        <p><strong>Emily R.</strong></p>
                        <p>"TinyCare has been a lifesaver for our family! Finding a reliable babysitter was never this easy. We love the detailed profiles and reviews."</p>
                    </div>
                    <div className="testimonial">
                        <p><strong>John D.</strong></p>
                        <p>"The booking process on TinyCare is seamless. We found an amazing babysitter who our kids adore. Highly recommend this service to other parents!"</p>
                    </div>
                    <div className="testimonial">
                        <p><strong>Linda K.</strong></p>
                        <p>"I appreciate the thorough vetting process TinyCare uses for their babysitters. It gives me peace of mind knowing my children are in safe hands."</p>
                    </div>
                </section>
            </div>
        </div>
    );
};

export default HomePage;
