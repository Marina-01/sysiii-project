import React, { useState, useEffect } from 'react';
import axios from 'axios';
import NavBar from './NavBar.js';
import { useNavigate } from 'react-router-dom'; 
import './MyProfileView.css'; // 

const MyProfileView = () => {
  const navigate = useNavigate();
  const [profile, setProfile] = useState(null);
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(true);
  const [editMode, setEditMode] = useState(false);
  const [editedProfile, setEditedProfile] = useState({});


  useEffect(() => {
    const fetchProfile = async () => {
      try {
        const token = localStorage.getItem('token');
        if (!token) {
          setError('Please Log In');
          setLoading(false);
          return;
        }

        const response = await axios.get('http://localhost:5000/api/myprofile', {
          headers: {
            Authorization: `Bearer ${token}`,
          },
        });

        setProfile(response.data);
        setEditedProfile(response.data);
        setLoading(false);
      } catch (err) {
        setError('Error fetching profile');
        setLoading(false);
        console.error('Fetch profile error:', err);
      } 
    };

    fetchProfile();
  }, []);

    const handleEditChange = (e) => {
    const { name, value } = e.target;
    setEditedProfile((prevProfile) => ({
      ...prevProfile,
      [name]: value,
    }));
  };

const handleSaveChanges = async () => {
    const token = localStorage.getItem('token');
    if (!token) {
      alert('No token found');
      return;
    }

    try {
      const response = await axios.put('http://localhost:5000/api/myprofile', editedProfile, {
        headers: { Authorization: `Bearer ${token}` }
      });

      console.log('Profile updated:', response.data);
      //alert('Profile updated successfully');
      setProfile(editedProfile); // Update profile state with new data
      setEditMode(false);
    } catch (err) {
      console.error('Error updating profile:', err.response?.data || err.message);
      setError('Failed to update profile');
    }
  };

    const handleLogout = () => {
        localStorage.removeItem('token'); // Clear the token
        window.location.href = '/sign-in'; // Redirect to login page
    };

       const handleLoginRedirect = () => {
        window.location.href = '/sign-in'; // Redirect to login page
    };


    if (error) {
        return (
            <div>
                <NavBar />
                <h1>{error}</h1>
                {error === 'Please log in'} {(
                    <button onClick={handleLoginRedirect}>Log In</button>
                )}
            </div>
        );
    }

  if (loading) {
    return <div>Loading...</div>;
  }

  if (error) {
    return <div>{error}</div>;
  }

    const handleDeleteAccount = async () => {
        const token = localStorage.getItem('token');
        if (!token) {
            alert('No token found');
            return;
        }

        try {
            const response = await axios.delete('http://localhost:5000/api/myprofile', {
                headers: { Authorization: `Bearer ${token}` }
            });
            console.log('Account deleted:', response.data);
            alert('Account deleted successfully');
            localStorage.removeItem('token'); // Remove token on successful delete
            navigate('/sign-in'); // Redirect to sign-in page
        } catch (err) {
            console.error('Error deleting account:', err.response?.data || err.message);
            setError('Failed to delete account');
        }
    };

  if (!profile) {
    return (
      <div>
        <NavBar />
        <h1>No profile data available</h1>
      </div>
    );
  }

return (
    <div>
      <NavBar />
      <h1>Profile</h1>
      {editMode ? (
        <div>
          <h2>Edit Profile</h2>
          <form>
          <div className="form-group">
            <label>
              Email:
              <input
                type="email"
                name="email"
                value={editedProfile.email || ''}
                onChange={handleEditChange}
              />
            </label>
             </div>
      <div className="form-group">
            <label>
              Username:
              <input
                type="text"
                name="username"
                value={editedProfile.username || ''}
                onChange={handleEditChange}
              />
            </label>
             </div>
      <div className="form-group">
            <label>
              Name:
              <input
                type="text"
                name="name"
                value={editedProfile.name || ''}
                onChange={handleEditChange}
              />
            </label>
             </div>
      <div className="form-group">
            <label>
              Surname:
              <input
                type="text"
                name="surname"
                value={editedProfile.surname || ''}
                onChange={handleEditChange}
              />
            </label>
             </div>
      <div className="form-group">
            <label>
              Phone:
              <input
                type="text"
                name="phone"
                value={editedProfile.phone || ''}
                onChange={handleEditChange}
              />
            </label>
             </div>
      <div className="form-group">
            <label>
              Role:
              <input
                type="text"
                name="role"
                value={editedProfile.role || ''}
                onChange={handleEditChange}
              />
              </label>
              </div>
            
            <button type="button" className="edit-profile-button" onClick={handleSaveChanges}>Save Changes</button>
            <button type="button"  className="cancel-button" onClick={() => setEditMode(false)}>Cancel</button>
          </form>
        </div>
      ) : (
        <div>
          <p>Email: {profile.email}</p>
          <p>Username: {profile.username}</p>
          <p>Name: {profile.name}</p>
          <p>Surname: {profile.surname}</p>
          <p>Phone: {profile.phone}</p>
          <p>Role: {profile.role}</p>
          {profile.children && profile.children.length > 0 && (
            <div>
              <h2>Children</h2>
              <ul>
                {profile.children.map((child) => (
                  <li key={child.id}>
                    Age: {child.age}, Gender: {child.gender}, Allergies: {child.allergies}
                  </li>
                ))}
              </ul>
            </div>
          )}
          <button onClick={() => setEditMode(true)} className="edit-profile-button">
            Edit Profile
          </button>
          <button onClick={handleDeleteAccount} className="delete-account-button">
            Delete My Account
          </button>
          <button className="logout-button" onClick={handleLogout}>Log Out</button>
        </div>
      )}
    </div>
  );
};

export default MyProfileView;