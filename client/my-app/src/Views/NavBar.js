import React from "react";
import {Link} from "react-router-dom";
import "./NavBar.css"
import Logo from "./logo.png"


class NavBar extends React.Component {
  render() {
    return (
    <div id="navbar">
    <div id="navbar1">
    <img src={Logo} id="logo" alt="logo" />
    
      <div id="navbar4">
      <Link to="/" className="navlink">HOME</Link>
      <Link to="/search" className="navlink">SEARCH</Link>
      <Link to="/notifications" className="navlink">NOTIFICATION</Link>
      <Link to="/dashboard" className="navlink">DASHBOARD</Link>
        <Link to="/sign-in" className="navlink">SIGN-IN</Link>
        <Link to="/sign-up" className="navlink">SIGN-UP</Link>
        <Link to="/myprofile" id="profile" className="navlink">PROFILE</Link>
      
      </div>
      <div id="navbar2"></div>
      <div id="navbar3"></div>

    </div>
    </div>
       
      

    );
  }
}
export default NavBar;