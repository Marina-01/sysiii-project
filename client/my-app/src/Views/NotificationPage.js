import React, { useState, useEffect } from 'react';
import axios from 'axios';
import NavBar from './NavBar';
import './NotificationPage.css'; 

const NotificationsPage = () => {
    const [bookings, setBookings] = useState([]);
    const [error, setError] = useState(null);

    useEffect(() => {
        const fetchNotifications = async () => {
            try {
                const token = localStorage.getItem('token');
                const response = await axios.get('http://localhost:5000/notifications', {
                    headers: { Authorization: `Bearer ${token}` }
                });
                console.log('Fetched bookings:', response.data);
                setBookings(response.data);
            } catch (err) {
                console.error('Error fetching notifications:', err.response?.data || err.message);
                setError('Error fetching notifications');
            }
        };

        fetchNotifications();
    }, []);

    const handleConfirm = async (bookingId) => {
        try {
            const token = localStorage.getItem('token');
            await axios.post(`http://localhost:5000/booking/${bookingId}/confirm`, {}, {
                headers: { Authorization: `Bearer ${token}` }
            });
            //alert('Booking confirmed');
            setBookings(bookings.map(booking => 
                booking.id === bookingId ? { ...booking, status: 'confirmed' } : booking
            ));
        } catch (err) {
            console.error('Error confirming booking:', err.response?.data || err.message);
            alert('Failed to confirm booking');
        }
    };

    const handleReject = async (bookingId) => {
        try {
            const token = localStorage.getItem('token');
            await axios.post(`http://localhost:5000/notifications/${bookingId}/reject`, {}, {
                headers: { Authorization: `Bearer ${token}` }
            });
            //alert('Booking rejected');
            setBookings(bookings.map(booking => 
                booking.id === bookingId ? { ...booking, status: 'rejected' } : booking
            ));
        } catch (err) {
            console.error('Error rejecting booking:', err.response?.data || err.message);
            alert('Failed to reject booking');
        }
    };

    if (error) {
        return <div>{error}</div>;
    }

    return (
        <div>
            <NavBar />
            <h1>Booking Requests</h1>
            <ul>
                {bookings.map((booking) => (
                    <li key={booking.id}>
                        <p><strong>Location:</strong> {booking.location}</p>
                        <p><strong>Number of Children:</strong> {booking.children.length}</p>
                        <p><strong>Status:</strong> {booking.status.charAt(0).toUpperCase() + booking.status.slice(1)}</p>
                        {booking.status === 'pending' && (
                            <>
                                <button onClick={() => handleConfirm(booking.id)}>Confirm</button>
                                <button onClick={() => handleReject(booking.id)}>Reject</button>
                            </>
                        )}
                    </li>
                ))}
            </ul>
        </div>
    );
};

export default NotificationsPage;