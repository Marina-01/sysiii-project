import React, { useState } from 'react';
import axios from 'axios';
import NavBar from './NavBar.js'



function Register() {
    const [email, setEmail] = useState('');
    const [username, setUsername] = useState('');
    const [password, setPassword] = useState('');
    const [name, setName] = useState('');
    const [surname, setSurname] = useState('');
    const [phone, setPhone] = useState('');
    const [role, setRole] = useState('parent');
    const [gender, setGender] = useState('male');
    const [children, setChildren] = useState([{ age: '', gender: 'male', allergies: '' }]);
    const [picture, setPicture] = useState(null);
    const [message, setMessage] = useState('');
    const [location, setLocation] = useState(''); // New location state
    const [error, setError] = useState('');
    const [success, setSuccess] = useState('');




    // const handleFileChange = (event) => {
    //     setPicture(event.target.files[0]);
    // };

    const handleAddChild = () => {
        setChildren([...children, { age: '', gender: 'male', allergies: '' }]);
    };

    const handleChildChange = (index, field, value) => {
        const newChildren = [...children];
        newChildren[index][field] = value;
        setChildren(newChildren);
    };

    const handleRegister = async (event) => {
        event.preventDefault();

        const formData = new FormData();
        formData.append('email', email);
        formData.append('username', username);
        formData.append('password', password);
        formData.append('name', name);
        formData.append('surname', surname);
        formData.append('phone', phone);
        formData.append('role', role);
        formData.append('gender', gender);
        //formData.append('picture', picture);
        formData.append('location', location);

        if (role === 'parent') {
            formData.append('children', JSON.stringify(children));
        }

        try {
            const response = await axios.post('http://localhost:5000/register', formData, {
                headers: { 'Content-Type': 'multipart/form-data' },
            });
            setMessage(response.data.message);
        } catch (error) {
            setMessage(error.response.data.message);
        }
    };

    return (
        <div>
        <NavBar /> 
            <h1>Register</h1>
            <form onSubmit={handleRegister}>
                <input
                    type="email"
                    placeholder="Email"
                    value={email}
                    onChange={(e) => setEmail(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Username"
                    value={username}
                    onChange={(e) => setUsername(e.target.value)}
                />
                <input
                    type="password"
                    placeholder="Password"
                    value={password}
                    onChange={(e) => setPassword(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Name"
                    value={name}
                    onChange={(e) => setName(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Surname"
                    value={surname}
                    onChange={(e) => setSurname(e.target.value)}
                />
                <input
                    type="text"
                    placeholder="Phone Number"
                    value={phone}
                    onChange={(e) => setPhone(e.target.value)}
                />

                <input
                    type="text"
                    placeholder="Location"
                    value={location}
                    onChange={(e) => setLocation(e.target.value)}
                    required
                />


                <select value={gender} onChange={(e) => setGender(e.target.value)}>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="other">Other</option>
                </select>

                <select value={role} onChange={(e) => setRole(e.target.value)}>
                    <option value="parent">Parent</option>
                    <option value="babysitter">Babysitter</option>
                </select>

                {role === 'parent' && (
                    <div>
                        <h3>Children Information</h3>
                        {children.map((child, index) => (
                            <div key={index}>
                                <input
                                    type="number"
                                    placeholder="Child's Age"
                                    value={child.age}
                                    onChange={(e) => handleChildChange(index, 'age', e.target.value)}
                                />
                                <select
                                    value={child.gender}
                                    onChange={(e) => handleChildChange(index, 'gender', e.target.value)}
                                >
                                    <option value="male">Male</option>
                                    <option value="female">Female</option>
                                    <option value="other">Other</option>
                                </select>
                                <input
                                    type="text"
                                    placeholder="Allergies"
                                    value={child.allergies}
                                    onChange={(e) => handleChildChange(index, 'allergies', e.target.value)}
                                />
                            </div>
                        ))}
                        <button type="button" onClick={handleAddChild}>
                            Add Another Child
                        </button>
                    </div>
                )}

                
                <button type="submit">Register</button>
            </form>
            {message && <p>{message}</p>}
        </div>
    );
}

export default Register;
