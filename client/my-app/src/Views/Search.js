import React, { useState, useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import axios from 'axios';
import NavBar from './NavBar';
import './Search.css'; 
import male from "./male.png";
import female from "./female.png";
import other from "./other.png";
import deff from "./default.png";

const SearchPage = () => {
    const [query, setQuery] = useState('');
    const [results, setResults] = useState([]);
    const [error, setError] = useState(null);
    const [isLoggedIn, setIsLoggedIn] = useState(false);
    const [showBookingForm, setShowBookingForm] = useState(false);
    const [selectedBabysitter, setSelectedBabysitter] = useState(null);
    const [bookingDetails, setBookingDetails] = useState({
        location: '',
        children: [{ age: '' }],
    });
    const navigate = useNavigate();
    const [bookingConfirmed, setBookingConfirmed] = useState(false);
    const [rating, setRating] = useState(0);
    const [confirmationMessage, setConfirmationMessage] = useState('');

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (!token) {
            setError('Please log in');
            setIsLoggedIn(false);
        } else {
            setIsLoggedIn(true);
            fetchBabysitters(token);
        }
    }, []);


const handleRateBabysitter = async (babysitterId, rating) => {
        const token = localStorage.getItem('token');
        if (!token) {
            alert('Please log in to rate');
            return;
        }

        try {
            await axios.post(`http://localhost:5000/rate/${babysitterId}`, { rating }, {
                headers: { Authorization: `Bearer ${token}` }
            });
            //alert('Rating submitted successfully');
        } catch (err) {
            console.error('Error rating babysitter:', err.response?.data || err.message);
            //alert('Failed to submit rating');
        }
};

const handleRatingChange = (babysitterId, newRating) => {
        setResults(results.map(babysitter =>
            babysitter.id === babysitterId
                ? { ...babysitter, currentRating: newRating }
                : babysitter
        ));
};

const fetchBabysitters = async (token) => {
        try {
            const response = await axios.get('http://localhost:5000/search', {
                headers: { Authorization: `Bearer ${token}` }
            });
            console.log('Fetched data:', response.data);
            setResults(response.data.map(babysitter => ({
                ...babysitter,
                currentRating: 0 // Initialize each babysitter with a default rating
            })));
            const babysitters = response.data;
            setResults(babysitters);
            setError(null);
        } catch (err) {
            console.error('Error fetching search results:', err.response?.data || err.message);
            setError('Error fetching search results');
        }
};

// const handleRatingSubmit = async () => {
//     try {
//         await axios.post(`http://localhost:5000/rate/${user.id}`, { rating }, {
//             headers: { Authorization: `Bearer ${token}` }
//         });
//         alert('Rating submitted successfully');
//     } catch (error) {
//         console.error('Error submitting rating:', error);
//     }
// };

    const handleSubmitRating = (babysitterId) => {
        const babysitter = results.find(b => b.id === babysitterId);
        if (babysitter) {
            handleRateBabysitter(babysitterId, babysitter.currentRating);
        } else {
            console.error('Babysitter not found');
        }
    };


const handleSubmitBooking = async (babysitterId) => {
    const token = localStorage.getItem('token');
    if (!token) {
        alert('No token found');
        return;
    }

    const bookingData = {
        location: bookingDetails.location,
        children: bookingDetails.children
    };

    try {
        const response = await axios.post(`http://localhost:5000/book/${babysitterId}`, bookingData, {
            headers: { Authorization: `Bearer ${token}` }
        });
        console.log('Booking successful:', response.data);
        
         // Calculate the number of children correctly
            const numChildren = bookingDetails.children.length;
            const childAges = bookingDetails.children.map(child => child.age).join(', ');

        // Set the confirmation message
            setConfirmationMessage(`Location: ${bookingDetails.location}\nNumber of Children: ${numChildren}\nAges of Children: ${childAges}`);
            setBookingConfirmed(true);  // Set the booking as confirmed
            setShowBookingForm(false)
    } catch (err) {
        console.error('Error submitting booking request:', err.response?.data || err.message);
        alert('Failed to send booking request');
    }
};


    const handleBookingFormChange = (index, field, value) => {
        const updatedChildren = [...bookingDetails.children];
        updatedChildren[index][field] = value;
        setBookingDetails({ ...bookingDetails, children: updatedChildren });
    };

    const addChildField = () => {
        setBookingDetails({
            ...bookingDetails,
            children: [...bookingDetails.children, { age: '' }]
        });
    };

    const getGenderImage = (gender) => {
        switch (gender) {
            case 'male':
                return male;
            case 'female':
                return female;
            case 'other':
                return other;
            default:
                return deff;
        }
    };

    const handleLoginRedirect = () => {
        navigate('/sign-in'); // Redirect to the sign-in page
    };

function Search({ babysitterId }) {
    const [averageRating, setAverageRating] = useState(null);

    useEffect(() => {
        if (babysitterId) {
            fetch(`/babysitter/${babysitterId}`)
                .then(response => response.json())
                .then(data => setAverageRating(data.averageRating))
                .catch(error => console.error('Error fetching average rating:', error));
        }
    }, [babysitterId]);

    return (
        <div>
            <p>Average Rating: {averageRating !== null ? averageRating : 'Loading...'}</p>
        </div>
    );
}
 return (
        <div>
            <NavBar />
            <h1>Babysitters</h1>
            {error && <div>{error}</div>}
            <div className="babysitter-grid">
                {results.map((babysitter) => (
                    <div key={babysitter.id} className="babysitter-card">
                        <div className="icon-placeholder">
                            <img 
                                src={getGenderImage(babysitter.gender || 'default')} 
                                alt={`${babysitter.username}'s profile`} 
                                className="profile-icon" 
                            />
                        </div>
                        <div className="babysitter-info">
                            <p><strong>{babysitter.name} {babysitter.surname}</strong></p>
                            <p>Username: {babysitter.username}</p>
                            <p>Location: {babysitter.location}</p>
                            <p>Phone: {babysitter.number || 'Not provided'}</p>
                            <p>Role: {babysitter.role}</p>

                            <p>Average Rating: Loading...</p>
                            <div className="rating-form">
                                <label>
                                    Rating (1-5):
                                    <input
                                        type="number"
                                        min="1"
                                        max="5"
                                        value={babysitter.currentRating}
                                        onChange={(e) => handleRatingChange(babysitter.id, Number(e.target.value))}
                                    />
                                </label>
                                <button onClick={() => handleSubmitRating(babysitter.id)}>Submit Rating</button>
                            </div>
                            <button onClick={() => {
                                setSelectedBabysitter(babysitter);
                                setShowBookingForm(true);
                                setBookingConfirmed(false);
                            }}>Book Now</button>
                        </div>
                    </div>
                ))}
            </div>

            {showBookingForm && selectedBabysitter && (
                <div className="booking-form">
                    <h2>Booking Request for {selectedBabysitter.name}</h2>
                    <label>
                        Location:
                        <input
                            type="text"
                            value={bookingDetails.location}
                            onChange={(e) => setBookingDetails({ ...bookingDetails, location: e.target.value })}
                        />
                    </label>
                    <div>
                        {bookingDetails.children.map((child, index) => (
                            <div key={index}>
                                <label>
                                    Child {index + 1} Age:
                                    <input
                                        type="number"
                                        value={child.age}
                                        onChange={(e) => handleBookingFormChange(index, 'age', e.target.value)}
                                    />
                                </label>
                            </div>
                        ))}
                        <button onClick={addChildField}>Add Another Child</button>
                    </div>
                    <button onClick={() => handleSubmitBooking(selectedBabysitter.id)}>Book Now</button>
                    <button onClick={() => setShowBookingForm(false)}>Cancel</button>
                </div>
            )}
            {bookingConfirmed && (
                <div className="booking-confirmation">
                    <h3>Booking Confirmed!</h3>
                    <p>Your booking has been successfully confirmed.</p>
                </div>
            )}
        </div>
    );
};

export default SearchPage;
