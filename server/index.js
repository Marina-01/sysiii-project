const express = require('express');
//const db = require('./db'); 
const authenticateToken = (req, res, next) => {
    const token = req.header('Authorization')?.split(' ')[1];

    if (!token) {
        return res.status(401).json({ error: 'No token provided' });
    }

    jwt.verify(token, secretKey, (err, user) => {
        if (err) {
            console.error('Token verification error:', err); // More detailed logging
            return res.status(403).json({ error: 'Invalid token' });
        }

        req.user = user;  // Attach user to request
        next();
    });
};

const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');
const multer = require('multer');
const path = require('path');
const cors = require('cors');
const sqlite3 = require('sqlite3').verbose();

//const router = express.Router();


// Create a new Express application
const app = express();
const PORT = 5000;

// Configure middleware
app.use(cors());
app.use(express.json());
app.use(authenticateToken)

// Initialize SQLite database
const db = new sqlite3.Database('users.db');

    // Define default pictures
    const defaultPictures = {
    male: "./View/male.png",
    female: "./View/female.png",
    other: "./View/other.png",
  default: "./View/default.png"
    };


// Create users table if it doesn't exist
// Create users table if it doesn't exist
db.serialize(() => {
    // Create users table
    db.run(`
        CREATE TABLE IF NOT EXISTS users (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            email TEXT UNIQUE NOT NULL,
            username TEXT UNIQUE NOT NULL,
            password TEXT NOT NULL,
            picture TEXT,
            name TEXT,
            surname TEXT,
            phone TEXT,
            role TEXT
        )
    `);

    // Create children table
    db.run(`
        CREATE TABLE IF NOT EXISTS children (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            parent_id INTEGER,
            age INTEGER,
            gender TEXT,
            allergies TEXT,
            FOREIGN KEY (parent_id) REFERENCES users(id)
        )
    `);

    // Create ratings table
    db.run(`
        CREATE TABLE IF NOT EXISTS ratings (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_id INTEGER,
            rating REAL NOT NULL,
            FOREIGN KEY (user_id) REFERENCES users(id)
        )
    `);

    // Create average_rating table
    db.run(`
        CREATE TABLE IF NOT EXISTS average_rating (
            id INTEGER PRIMARY KEY AUTOINCREMENT,
            user_id INTEGER,
            average_rating REAL,
            FOREIGN KEY (user_id) REFERENCES users(id)
        )
    `);
});




// Middleware to check authentication
const authMiddleware = (req, res, next) => {
    const token = req.headers.authorization?.split(' ')[1]; // Bearer token

    if (!token) {
        return res.status(403).json({ message: 'No token provided' });
    }

    jwt.verify(token, 'your_secret_key', (err, decoded) => {
        if (err) {
            return res.status(403).json({ message: 'Failed to authenticate token' });
        }
        
        req.user = decoded; // Attach decoded user info to request object
        next();
    });
};


// Set up storage configuration for multer
const storage = multer.diskStorage({
    destination: (req, file, cb) => {
        cb(null, 'uploads/'); // Save files to the 'uploads' directory
    },
    filename: (req, file, cb) => {
        const ext = path.extname(file.originalname);
        cb(null, Date.now() + ext); // Append timestamp to avoid name collisions
    }
});

const upload = multer({ storage });

// // Secret key for JWT
const secretKey = 'your-secret-key';

// function authenticateToken(req, res, next) {
//     const token = req.header('Authorization')?.split(' ')[1];
//     if (!token) return res.sendStatus(401);

//     jwt.verify(token, process.env.JWT_SECRET, (err, user) => {
//         if (err) return res.sendStatus(403);
//         req.user = user;
//         next();
//     });
// }

//const jwt = require('jsonwebtoken');



// Registration route
app.post('/register', upload.single('picture'), async (req, res) => {
    const { username, password, email, gender, name, surname, phone, role, number, location } = req.body;
    const profilePicture = defaultPictures[gender] || defaultPictures.default;
    
    console.log('Request Body:', req.body); 
    // Check if user already exists
    db.get('SELECT * FROM users WHERE username = ? OR email = ?', [username, email], async (err, row) => {
        if (err) {
            return res.status(500).json({ message: 'Database query error' });
        }

        if (row) {
            return res.status(400).json({ message: 'User with this email or username already exists' });
        }

        // Hash the password
        const hashedPassword = await bcrypt.hash(password, 10);

        // Insert new user
        console.log('Profile Picture:', profilePicture); 
        db.run(`INSERT INTO users (username, password, email, number, name, surname, role, location) VALUES (?, ?, ?, ?, ?, ?, ?, ?)`,
        [username, hashedPassword, email, profilePicture, name, surname, phone, role, number, location],
            function (err) {
                if (err) {
                  console.error('Database insert error:', err); 
                    return res.status(500).json({ message: 'Database insert error' });
                }

                const userId = this.lastID;

                // If the role is parent, insert children information
                if (role === 'parent' && children && children.length > 0) {
                    const childInsertPromises = children.map(child => {
                        return new Promise((resolve, reject) => {
                            db.run(
                                'INSERT INTO children (parent_id, age, gender, allergies) VALUES (?, ?, ?, ?)',
                                [userId, child.age, child.gender, child.allergies],
                                (err) => {
                                    if (err) reject(err);
                                    else resolve();
                                }
                            );
                        });
                    });

                    Promise.all(childInsertPromises)
                        .then(() => {
                            res.status(201).json({ message: 'User registered successfully with children data' });
                        })
                        .catch(() => {
                            res.status(500).json({ message: 'Error inserting children data' });
                        });
                } else {
                    res.status(201).json({ message: 'User registered successfully' });
                }
            }
        );
    });
});

// Login route
app.post('/login', async (req, res) => {
    const { email, password } = req.body;

    // Find user by email
    db.get('SELECT * FROM users WHERE email = ?', [email], async (err, row) => {
        if (err) {
            return res.status(500).json({ message: 'Database query error' });
        }

        if (!row) {
            return res.status(400).json({ message: 'User does not exist' });
        }

        // Check password
        const isPasswordValid = await bcrypt.compare(password, row.password);
        if (!isPasswordValid) {
            return res.status(400).json({ message: 'Invalid password' });
        }

        // Generate JWT token
        const token = jwt.sign({ email: row.email, username: row.username, role: row.role, id: row.id }, secretKey, { expiresIn: '1h' });

        res.json({ token });
    });
});

// Profile route
app.get('/api/myprofile', async (req, res) => {
//app.get('/myprofile', (req, res) => {
    const authHeader = req.headers.authorization;

    if (!authHeader) {
        console.log('Authorization header is missing'); // Debug log
        return res.status(401).json({ message: 'Authorization header is missing' });
    }

    const token = authHeader.split(' ')[1];
    console.log('Received Token:', token); // Debug log

    jwt.verify(token, secretKey, (err, decoded) => {
        if (err) {
            console.log('Token verification failed:', err); // Debug log
            return res.status(401).json({ message: 'Invalid token' });
        }

        const email = decoded.email;
        console.log('Decoded email:', email); // Debug log

        db.get('SELECT * FROM users WHERE email = ?', [email], (err, row) => {
            if (err) {
                console.log('Database query error:', err); // Debug log
                return res.status(500).json({ message: 'Database query error' });
            }

            if (!row) {
                console.log('User not found'); // Debug log
                return res.status(404).json({ message: 'User not found' });
            }

            db.all('SELECT * FROM children WHERE parent_id = ?', [row.id], (err, children) => {
                if (err) {
                    console.log('Error retrieving children data:', err); // Debug log
                    return res.status(500).json({ message: 'Error retrieving children data' });
                }

                const userProfile = {
                    id: row.id,
                    email: row.email,
                    username: row.username,
                    name: row.name,
                    surname: row.surname,
                    phone: row.phone,
                    role: row.role,
                    gender: row.gender,
                    picture: row.picture,
                    children: children
                };

                console.log('User Profile:', userProfile); // Debug log
                res.json(userProfile);
            });
        });
    });
});

// Serve static files from the React app
// app.use(express.static(path.join(__dirname, 'client/my-app/build')));

// // Catch-all handler to serve the React app for any route not handled by an API route
// app.get('*', (req, res) => {
//     res.sendFile(path.join(__dirname, 'client/my-app/build', 'index.html'));
// });

// Function to update profile in database
const updateProfileInDatabase = (profile) => {
  return new Promise((resolve, reject) => {
    const { id, email, username, name, surname, phone, role } = profile;

    db.run(
      `UPDATE users SET email = ?, username = ?, name = ?, surname = ?, phone = ?, role = ? WHERE id = ?`,
      [email, username, name, surname, phone, role, id],
      function (err) {
        if (err) {
          reject(err);
        } else {
          resolve();
        }
      }
    );
  });
};

// Search users route
app.get('/search', authenticateToken, async (req, res) => {
    try {
        const { query } = req.query;
        const userId = req.user.id;  // Get the logged-in user's ID from the token

        let sqlQuery = `
            SELECT id, email, username, number, name, surname, role, location 
            FROM users 
            WHERE role = ? 
            AND id != ?`;  // Exclude the current user's profile

        let params = ['babysitter', userId];

        if (query) {
            sqlQuery += ` AND (username LIKE ? OR email LIKE ? OR name LIKE ? OR surname LIKE ?)`;
            params.push(`%${query}%`, `%${query}%`, `%${query}%`, `%${query}%`);
        }

        db.all(sqlQuery, params, (err, rows) => {
            if (err) {
                console.log('Database query error:', err);
                return res.status(500).json({ message: 'Database query error' });
            }
            res.json(rows);
        });
    } catch (err) {
        console.log('Error in /search route:', err);
        res.status(500).json({ message: 'Internal server error' });
    }
});





app.post('/book/:id', authenticateToken, (req, res) => {
    const { id } = req.params;  // Babysitter ID
    const { location, children } = req.body;
    
    const userId = req.user.id;  // Extracted from the token
    
    const query = `INSERT INTO bookings (babysitter_id, user_id, location, children) VALUES (?, ?, ?, ?)`;
    const params = [id, userId, location, JSON.stringify(children)];
    
    db.run(query, params, function (err) {
        if (err) {
            console.error("Database error:", err.message);
            res.status(500).json({ error: "Database error" });
        } else {
            res.status(201).json({ success: true, bookingId: this.lastID });
        }
    });
});



app.post('/book/:babysitterId', authenticateToken, (req, res) => {
    if (!req.user) {
        return res.status(401).json({ message: 'Unauthorized: User information not found' });
    }

    const userId = req.user.id;
    const babysitterId = req.params.babysitterId;
    const { location, children } = req.body;

    if (!location || !children) {
        return res.status(400).json({ message: 'Location and children details are required' });
    }

    const query = `
        INSERT INTO bookings (babysitter_id, user_id, location, children)
        VALUES (?, ?, ?, ?)
    `;

    db.run(query, [babysitterId, userId, location, JSON.stringify(children)], function(err) {
        if (err) {
            console.error('Database query error:', err);
            return res.status(500).json({ message: 'Database query error' });
        }
        res.status(201).json({ message: 'Booking request submitted', bookingId: this.lastID });
    });
});


app.get('/notifications', authenticateToken, (req, res) => {
     console.log('Request user:', req.user);

    if (!req.user || !req.user.id) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    const userId = req.user.id;

    db.all('SELECT * FROM bookings WHERE babysitter_id = ? AND status = "pending"', [userId], (err, rows) => {
        if (err) {
            console.error('Database query error:', err);
            return res.status(500).json({ message: 'Database query error' });
        }

        res.json(rows);
    });


});


app.post('/booking/:bookingId/confirm', authMiddleware, (req, res) => {
    const bookingId = req.params.bookingId;
    const userId = req.user.id; // Assuming userId is available from authMiddleware

    db.run('UPDATE bookings SET status = "confirmed" WHERE id = ? AND user_id = ?', [bookingId, userId], function(err) {
        if (err) {
            console.log('Database update error:', err);
            return res.status(500).json({ message: 'Failed to confirm booking' });
        }

        if (this.changes === 0) {
            return res.status(404).json({ message: 'Booking not found or not authorized' });
        }

        res.json({ message: 'Booking confirmed' });
    });
});


app.post('/booking/:bookingId/reject', authMiddleware, (req, res) => {
    const bookingId = req.params.bookingId;
    const userId = req.user.id; // Assuming userId is available from authMiddleware

    db.run('UPDATE bookings SET status = "rejected" WHERE id = ? AND user_id = ?', [bookingId, userId], function(err) {
        if (err) {
            console.log('Database update error:', err);
            return res.status(500).json({ message: 'Failed to reject booking' });
        }

        if (this.changes === 0) {
            return res.status(404).json({ message: 'Booking not found or not authorized' });
        }

        res.json({ message: 'Booking rejected' });
    });
});


app.delete('/api/myprofile', authenticateToken, (req, res) => {
    const userId = req.user.id;

    // Delete user and their children data
    db.serialize(() => {
        db.run('DELETE FROM children WHERE parent_id = ?', [userId], (err) => {
            if (err) {
                console.error('Error deleting children data:', err);
                return res.status(500).json({ message: 'Failed to delete children data' });
            }

            db.run('DELETE FROM users WHERE id = ?', [userId], function (err) {
                if (err) {
                    console.error('Error deleting user:', err);
                    return res.status(500).json({ message: 'Failed to delete user' });
                }

                res.status(200).json({ message: 'User deleted successfully' });
            });
        });
    });
});


// PUT request handler for updating profile
app.put('/api/myprofile', authenticateToken, async (req, res) => {
  try {
    const updatedProfile = req.body;
    await updateProfileInDatabase(updatedProfile);
    res.json({ message: 'Profile updated successfully' });
  } catch (err) {
    console.error('Error updating profile:', err);
    res.status(500).json({ error: 'Failed to update profile' });
  }
});



module.exports = authMiddleware;

// Example route for fetching user bookings
app.get('/notifications', authMiddleware, async (req, res) => {
    try {
        const userId = req.user.id; // Assuming `req.user.id` is set by your authentication middleware
        const bookings = await Booking.find({ userId }); // Fetch bookings for the logged-in user
        res.json(bookings);
    } catch (error) {
        console.error('Error fetching bookings:', error);
        res.status(500).send('Error fetching bookings');
    }
});
//module.exports = router;


// Endpoint to get the user's bookings
app.get('/my-bookings', authMiddleware, (req, res) => {
    const userId = req.user.id; // Extract user ID from the request (set by authMiddleware)

    db.all('SELECT * FROM bookings WHERE user_id = ?', [userId], (err, rows) => {
        if (err) {
            console.error('Database query error:', err);
            return res.status(500).json({ message: 'Failed to fetch bookings' });
        }
        res.json(rows);
    });
});

// Endpoint to fetch bookings for the authenticated user
app.get('/my-bookings', authMiddleware, (req, res) => {
    const userId = req.user.id; // Get the user ID from the authenticated request

    db.all('SELECT * FROM bookings WHERE user_id = ?', [userId], (err, rows) => {
        if (err) {
            console.error('Database query error:', err);
            return res.status(500).json({ message: 'Failed to fetch bookings' });
        }
        res.json(rows);
    });
});

// POST /rate/:babysitterId
app.post('/rate/:babysitterId', authenticateToken, (req, res) => {
    const { babysitterId } = req.params;
    const { rating } = req.body;
    const parentId = req.user.id;

    if (!parentId) {
        return res.status(401).json({ message: 'Unauthorized' });
    }

    // Insert rating into the database
    const sql = 'INSERT INTO ratings (babysitter_id, parent_id, rating) VALUES (?, ?, ?)';
    db.run(sql, [babysitterId, parentId, rating], function (err) {
        if (err) {
            console.error('Error inserting rating:', err.message);
            return res.status(500).json({ message: 'Failed to submit rating' });
        }
        res.status(200).json({ message: 'Rating submitted successfully' });
    });
});

// GET /babysitter/:id
app.get('/babysitter/:id', (req, res) => {
    const userId = req.params.id;

    db.get(`
        SELECT AVG(rating) AS avg_rating
        FROM ratings
        WHERE user_id = ?
    `, [userId], (err, row) => {
        if (err) {
            res.status(500).json({ error: 'Error retrieving average rating' });
            return;
        }

        const averageRating = row.avg_rating;

        res.json({ averageRating });
    });
});




// Start the server
app.listen(PORT, () => {
    console.log(`Server is running on http://localhost:${PORT}`);
});
